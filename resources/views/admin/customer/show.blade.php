@extends('admin.customer.layout')

@section('content')
<x-app-layout>
   
<style type="text/css">
    .col-md-6
    {
        color: blue;

    }
    .card
    {
        justify-content: center;
        text-align: center;
    }
</style>
 


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                    <div class="card-header">View customer Details</div>
            <div class="card-body">
                 @if(Session::has('success'))
                 <div class="alert alert-success">
        {{Session::get('success')}}
    </div>
@endif
@if (session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@endif
            
    

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>FirstName</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $customer->FirstName }}</label>
                    </div>
                </div>

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>LastName</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $customer->LastName }}</label>
                    </div>
                </div>

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Address</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $customer->Address }}</label>
                    </div>
                </div>


                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Phone</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $customer->Phone }}</label>
                    </div>
                </div>
  <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>AdharCard</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $customer->AdharCard }}</label>
                    </div>
                </div>  <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>PanCard</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $customer->PanCard }}</label>
                    </div>
                </div>
                  <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Referenes</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $customer->Referenes }}</label>
                    </div>
                </div>
              

                <a href="{{ route('customer.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
</div>

</div>


</x-app-layout>
@endsection
