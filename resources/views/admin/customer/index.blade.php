
  <x-app-layout>

<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
</head>
<body>
    
<div class="container">
    <h1>customer page</h1>
   @if(Session::has('success'))
    <div class="alert alert-success">
        {{Session::get('success')}}
    </div>
@endif

@if (session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@endif
    <div class="pull-right mb-2">
<a class="btn btn-success" onClick="add()"  href="{{ route('customer.create') }}">Add customer</a>
</div>
<div class="pull-right mb-2">
                    <a href="{{ route('CustomerController.export') }}" class="btn btn-info btn-sm">Export</a>
                  

                    <a href="{{ route('CustomerController.import-form') }}" class="btn btn-primary btn-sm">Import</a>
                  </div>
                  
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                     <th>No</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>Phone</th>
            <th>Address</th>
            <th>PanCard</th>
            <th>AdharCard</th>

            <th>Referenes</th>
                <th width="280px">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
   
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="productForm" name="productForm" class="form-horizontal">
                   <input type="hidden" name="product_id" id="product_id">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">FirstName</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="FirstName" name="FirstName" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">LastName</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="LastName" name="LastName" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="Address" name="Address" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Phone</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="Phone" name="Phone" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Adharcard</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="Adharcard" name="Adharcard" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Pancard</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="Pancard" name="Pancard" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Referenes</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="Referenes" name="Referenes" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
     
                  
      
                    <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                     </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
</body>
    
<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
     var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('customer.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'FirstName', name: 'FirstName'},
            {data: 'LastName', name: 'LastName'},
            {data: 'Address', name: 'Address'},
            {data: 'Phone', name: 'Phone'},
            {data: 'AdharCard', name: 'AdharCard'},
            {data: 'PanCard', name: 'PanCard'},
            {data: 'Referenes', name: 'Referenes'},


            {data: 'action', name: 'action', orderable: false, searchable: false},

        ]
    });
     
    $('#createNewProduct').click(function () {
        $('#saveBtn').val("create-product");
        $('#product_id').val('');
        $('#productForm').trigger("reset");
        $('#modelHeading').html("Create New Product");
        $('#ajaxModel').modal('show');
    });
    
    $('body').on('click', '.editProduct', function () {
      var product_id = $(this).data('id');
      $.get("{{ route('customer.index') }}" +'/' + product_id +'/edit', function (data) {
          $('#modelHeading').html("Edit Product");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#product_id').val(data.id);
          $('#name').val(data.name);
          $('#detail').val(data.detail);
      })
   });
    
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
    
        $.ajax({
          data: $('#productForm').serialize(),
          url: "{{ route('customer.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
     
              $('#productForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              table.draw();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });
    
    $('body').on('click', '.deleteProduct', function () {
     
        var product_id = $(this).data("id");
        confirm("Are You sure want to delete !");
      
        $.ajax({
            type: "DELETE",
            url: "{{ route('customer.store') }}"+'/'+product_id,
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
     
  });
</script>
</html>
</x-app-layout>