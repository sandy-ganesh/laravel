@extends('admin.customer.layout')

@section('content')



<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header">Import Product Form excel</div>
                <div class="card-body">
                    <form action="{{ route('CustomerController.import') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Choose Excel file</label><span class="text-danger">*</span>
                            <input type="file" name="file" class="form-control" >
                            <span class="text-danger">@error('file'){{ $message }}@enderror</span>
                        </div>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </form>
                  
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
