@extends('admin.categorys.layout')
  
@section('content')
<head>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
   
  
  


    
    
 

<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
    
</head>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('categorys.index') }}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('categorys.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
        </div>  
       
              
       
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description:</strong>
                <input type="text" class="form-control"  name="description" placeholder="Description">
            </div>
        </div>

       
  <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit"  class="btn btn-primary">Submit</button>
        </div>
    </div>
</div>
</div>









<!-- <script > 
    var cricket = $('#cricket').val();
console.log(cricket);

 var chess = $('#chess').val();
console.log(chess);
 var baseball = $('#baseball').val();
console.log(baseball);</script>
    -->  
      

   
</form>
@endsection