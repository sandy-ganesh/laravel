@extends('admin.categorys.layout')
  
@section('content')
    <style type="text/css">
    .container
    {
        margin-left: 60px;
        text-align: center;
        justify-content: center;
        padding: 50px 20px 30px 50px;
        margin: 25px;
        display: flex;
    }
</style>
<div class="container">
    <table class="table table-bordered">
     <thead>
        <tr>
            <td><strong>List of Categorys</td></strong>
        </tr>
    </thead>
    <tbody>
        <tr>
  
  <td>Name : {{ $category->name }} </td>
        </tr>
        <tr>
            <td>Description :  {{ $category->description }}</td>
        </tr>
       
            
            <td><a class="btn btn-primary" href="{{ route('categorys.index') }}"> Back</a></td>
        </tbody>
    </table>
</div>
</div>

@endsection

