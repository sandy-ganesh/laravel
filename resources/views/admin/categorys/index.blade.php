@extends('admin.categorys.layout')

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <style type="text/css">
    .container
    {
        margin: 50px;
        padding: 0px
    }
</style>

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>welcome</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('categorys.create') }}">Add</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Description</th>

            <th width="280px">Action</th>
        </tr>
        @foreach ($categorys as $product)
        <tr>

            <td>{{ ++$i }}</td>
            <td>{{ $product->name }}</td>
            
            <td>{{ $product->description }}</td>

            <td>
                <form action="{{ route('categorys.destroy',$product->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('categorys.show',$product->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('categorys.edit',$product->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
      

</x-app-layout>

