@extends('admin.products.layout')
@section('content')
  <x-app-layout>
<style type="text/css">
    .col-md-6
    {
        color: blue;

    }
    .card
    {
        justify-content: center;
        text-align: center;
    }
    
</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">View customer Details</div>
            <div class="card-body">

                <div class="row md-12">
                    <div class="col-md-3">
                        <label class="my-3"><b>Name</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $product->name }}</label>
                    </div>
                </div>

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Description</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $product->description }}</label>
                    </div>
                </div>

<div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Category_name</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{$product->category_name }}</label>
                    </div>
                
                </div>

        
        

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Price</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $product->price }}</label>
                    </div>
                </div>

                <a href="{{ route('products.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
</div>
</div>
</x-app-layout>
@endsection
