@extends('admin.customer.layout')
     
@section('content')

  <x-app-layout>
    
<!DOCTYPE html>
<html>
<head>
   <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
   
   
</head>
<body>
   <div class="container mt-2">
<div class="row">
<div class="col-lg-12 margin-tb">
<div class="pull-left">
<h2>CUSTOMER PAGE</h2>
</div>
<div class="pull-right mb-2">
<a class="btn btn-success" onClick="add()"  href="{{ route('customer.create') }}">Add customer</a>


                    
                </div>
                </div>
                 <div class="float-left">
                    <a href="{{ route('admin.customer.export') }}" class="btn btn-success mr-3">Export</a>
                </div>
               

</div>

  <div class="container">
    <table class="table table-bordered data-table" >
        <thead>
            <tr>
               <th>No</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>Phone</th>
            <th>Address</th>
            <th>PanCard</th>
            <th>AdharCard</th>

            <th>Referenes</th>
                <th width="100px">Action</th>
            </tr>

</thead>
        <tbody>
        </tbody>
    </table>
</div>

   
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
 <script type="text/javascript">

  $(function () {
     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('customer.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'FirstName', name: 'FirstName'},
            {data: 'LastName', name: 'LastName'},
            {data: 'Address', name: 'Address'},
            {data: 'Phone', name: 'Phone'},
            {data: 'AdharCard', name: 'AdharCard'},
            {data: 'PanCard', name: 'PanCard'},
            {data: 'Referenes', name: 'Referenes'},


            {data: 'action', name: 'action', orderable: false, searchable: false},

        ]
    });
   
   $('body').on('click', '.categorys', function () {
     
        var product_id = $(this).data("id");
        confirm("Are You sure want to delete !");
      
        $.ajax({
            type: "DELETE",
            url: "{{ route('customer.store') }}"+'/'+product_id,
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
  });
</script>
</html>
</x-app-layout>

@endsetion