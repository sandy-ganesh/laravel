@extends('admin.staffcustomer.layout')
     

    <x-app-layout>

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>customer</h2>
            </div>
           
        </div>
    </div>
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
     
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>Phone</th>
            <th>Address</th>
            <th>PanCard</th>
            <th>AdharCard</th>

            <th>Referenes</th>
            <th width="280px">Action</th>
        </tr>
        

        @foreach ($customer as $customer)
        <tr>


            <td>{{ $customer->id }}</td>

            <td>{{$customer->FirstName}}</td>
            <td>{{$customer->LastName}}</td>
            <td>{{$customer->Phone}}</td>

            <td>{{$customer->Address}}</td>
            <td>{{$customer->AdharCard}}</td>
            <td>{{$customer->PanCard}}</td>
            <td>{{$customer->Referenes}}</td>

            <td>
     
                   
      
                    <a class="btn btn-primary" href="{{ route('staffcustomer.edit',$customer->id) }}">Edit</a>
     
        
                </form>
            </td>
        </tr>
        @endforeach
    
    </table>
</x-app-layout>

    
        

