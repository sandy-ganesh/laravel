@extends('admin.staffcustomer.layout')
   
@section('content')

    <x-app-layout>

    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h2>Edit customer</h2>

           <div class="pull-right">

                <a class="btn btn-primary" href="{{ route('staffcustomer.index') }}"> Back</a>
            </div>
        </div>
    </div>
 
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('staffcustomer.update',$customer->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>FirstName:</strong>
                    <input type="text" name="FirstName" value="{{ $customer->FirstName }}" class="form-control" >
                </div>
            </div>
        </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">
                    <strong>LastName:</strong>
                    <input type="text" name="LastName" value="{{ $customer->LastName }}" class="form-control" >
                </div>
            </div>
        </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">

            
                <div class="form-group">
                    <strong>Address:</strong>
                    <input type="text" name="Address" value="{{ $customer->Address }}" class="form-control" >
                </div>
            </div>
        </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
            
                <div class="form-group">
                    <strong>Phone:</strong>
                    <input type="number" name="Phone" value="{{ $customer->Phone }}" class="form-control" >
                </div>
            </div>
        </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
        
                <div class="form-group">
                    <strong>PanCard:</strong>
                    <input type="number" name="PanCard" value="{{ $customer->PanCard }}" class="form-control" >
                </div>
            </div>
        </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
            
                <div class="form-group">
                    <strong>AdharCard:</strong>
                    <input type="number" name="AdharCard" value="{{ $customer->AdharCard }}" class="form-control" >
                </div>
            </div>
        </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
        
                <div class="form-group">
                    <strong>Reference:</strong>
                    <input type="text" name="Referenes" value="{{ $customer->Referenes }}" class="form-control" >
                </div>
            </div>
            </div>
           
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </div>
    
    
        
    </form>
</div>
</x-app-layout>
   

@endsection