<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\Staff1Controller;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CustomerExportController;
use App\Http\Controllers\CustomerImportController;

use App\Mail\WelcomeMail;


use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;


// use App\Http\Controllers\RoleController;
// use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/emails',function()
{
	Mail::to('sandysandyrg123@gmail.com')->send(new WelcomeMail());
	return new WelcomeMail();
});

// Route::get('/emails', function () {
   
//     $customer = [
//         'name' => 'Mail from ItSolutionStuff.com',
//         'info' => 'This is for testing email using smtp'
//     ];
   
//     \Mail::to('sandysandyrg123@gmail.com')->send(new WelcomeMail($customer));
   
// });

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
// Route::resource('admin/customer', CustomerController::class);


// Route::middleware(['auth:sanctum', 'verified'])->get('/customer.index', function () {
//     return view('customer');
// })->name('customer');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix'=>'admin', 'middleware'=>['Isadmin','auth']], function()

{
Route::get('/dashboard', [HomeController::class, 'index'])->name('home');


	Route::get('staff',[Staff1Controller::class,'index'])->name('staff');

	Route::resource('customer',CustomerController::class);

	
	Route::resource('categorys',CategoryController::class);
	Route::resource('products',ProductController::class);



  Route::get('/export', [CustomerController::class, 'export'])->name('CustomerController.export');
    Route::get('/import-form', [CustomerController::class, 'importform'])->name('CustomerController.import-form');
    Route::post('/import', [CustomerController::class, 'import'])->name('CustomerController.import');






// 
	// Route::get('customer',[CustomerController::class,'index']);


	
	
});

// Route::group(['middleware'=>['auth','Isstaff']],function()
Route::group(['prefix'=>'staff', 'middleware'=>['Isstaff','auth']], function()

{
// Route::get('/dashboard', [HomeController::class, 'staff'])->name('home');

	
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

	
	// Route::resource('customer',CustomerController::class,);
	// Route::get('customer',[CustomerController::class,'index'])->name('customer');

	Route::resource('staffcustomer',StaffController::class);



});
	
	
// });

// Route::group(['prefix'=>'admin', 'middleware'=>['Isadmin','auth']], function(){
//     Route::get('/staffindex', [App\Http\Controllers\StaffController::class, 'Isadmin'])->name('staff.index'); 
//  Route::get('/emphome', [App\Http\Controllers\HomeController::class, 'emp'])->name('emp.home'); // Employee Home

//     Route::resource('empproduct',EmpProductController::class); //Produ

// 	// Route::resource('staff',StaffController::class);

//     //
// });
// Route::group(['prefix'=>'admin', 'middleware'=>['Isadmin','auth']], function(){

//     Route::get('/staffrindex', [App\Http\Controllers\StaffController::class, 'staff'])->name('staff.index'); // Admin Home

//     Route::resource('staff',StaffController::class); 

// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
