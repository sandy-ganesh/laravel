<?php

namespace App\Imports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Database\Eloquent\ModelNotFoundException;



class CustomerImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
        private $rows = 0;

    public function model(array $row)
    {

      
              
try
{

                ++$this->rows;

                return new Customer([

                'FirstName'   => $row['firstname'],
                'LastName'   => $row['lastname'],
                'Address'   => $row['address'],
                'Phone'      => $row['phone'],
                'AdharCard'        => $row['adharcard'],
                'PanCard'      => $row['pancard'],
                 'Referenes'      => $row['referenes'],

            ]);

               
    
//      public function getRowCount(): int
//     {
//         return $this->rows;
//     }  
// }
// }
 } catch (ModelNotFoundException $exception) {
        return back()->withError($exception->getMessage())->withInput();
    }
}
}
