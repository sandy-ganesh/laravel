<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         "FirstName" => "required|min:02|max:30",
          "LastName"  => "required|min:02|max:30",
          "Address"  => "required|max:30",
          "Phone"  => "required|max:10",
          "AdharCard"  => "required|max:15",
          "PanCard"  => "required|max:10",
          "Referenes"  => "required|max:20",
        ];
    }
}
