<?php
  
namespace App\Http\Controllers;
  
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductCategory;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
       

   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {

        
       


        
   $product = DB::table('productcategory')
            ->join('products', 'products.id', '=', 'productcategory.product_id')
            ->join('categories', 'categories.id', '=', 'productcategory.category_id')
            ->select('products.*', 'categories.name as category_name')
            ->get();
           
        return view('admin.products.index',compact('product'));

        // $products = Product::latest()->paginate(5);
                // $products = Product::all(["id","name"]);

        // return view('admin.products.index',compact('products','category'));
       // return view('admin.products.index',compact('products'));

    
        // return view('products.index',compact('products'))
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('products.create');
        // $category = DB::table('categories')->pluck("name","id");
        $category=Category::all(["id","name"]);

        // return view('admin.products.create');
return view('admin.products.create',['category'=>$category]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //   $request->validate([
      //       'name' => 'required',
      //       'description' => 'required',
      //   ]);
  
      //   $input = $request->all();
      // Product::create($input);
         $product = new Product;
        $product->name   = $request['name'];
        $product->price   = $request['price'];
        $product->description   = $request['description'];
        $product->save();
        $product->Category()->attach($request->category_id);
     
        return redirect()->route('products.index')
                        ->with('success','Product created successfully.');
    }
     
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function show(Product $product)
    public function show($id)

    {
  $product = DB::table('productcategory')
            ->join('products', 'products.id', '=', 'productcategory.product_id')
            ->join('categories', 'categories.id', '=', 'productcategory.category_id')
            ->where('products.id',$id)
            ->select('products.*', 'categories.name as category_name')
            ->first();
            //print_r($product);
            //exit();
        return view('admin.products.show',compact('product'));
    }
     
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     // public function edit(Product $product)
     // {()

        // $category=Category::all(["id","name"]);
public function edit($id)
{
    $product = Product::where('id',$id)->select('*')->first();
    $product1 = DB::table('categories')->join('productcategory','categories.id','=','productcategory.category_id')->where('productcategory.product_id',$id)->pluck('categories.id')->toArray();
            $category = Category::all(["id","name"]);
              // print_r($category);
              // exit();  
     
       return view('admin.products.edit',compact('product','category','product1'));
}    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, Product $product)
    public function update(Request $request, $id)

    {
        $product = Product::find($id);
        $product->name   = $request['name'];
        $product->price   = $request['price'];
        $product->description   = $request['description'];
       $product->update();
        // Sync method
        $product->Category()->sync($request->category_id);
        
        // $product=Product::find($id);
        //             $category = Category::all(["id","name"]);

        // $input = $request->all();


        // $product->update($input);

        return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // public function destroy(Product $product)
    public function destroy($id)


    {
       $product=Product::find($id);
         // $product->active_status="0";
         // $product->delete_status="1";
        // $product->delete();
     
        return redirect()->route('products.index')
                        ->with('success','Product deleted successfully');
    }
}

