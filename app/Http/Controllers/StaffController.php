<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Repository\StaffInterface;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public $customer;

    //Here Using customer Repository
    public function __construct(StaffInterface $customer)
    {
        $this->customer = $customer;
    }
    public function index()
    {


        
        $customer= $this->customer->getAll();

        return view('admin.staffcustomer.index', compact('customer'));
       
        //  $customer = Customer::latest()->paginate(5);
    
        // return view('admin.staffcustomer.index',compact('customer'))
        //     ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $validator = \Validator::make($request->all(), [
             'FirstName' => 'required',
            'LastName' => 'required',
            'Address' => 'required',
            'Phone' => 'required',
            'AdharCard' => 'required',
            'PanCard' => 'required',
            'Referenes' => 'required',

        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        
        
        //staff::create($input);
       $input=  $this->customer->create(request()->all());
       return redirect()->route('staffcustomer.index')
            ->with('success', 'saved successfully');
         // Customer::create($request->all());
      // $customer = new customer;
      //    $customer->FirstName = $request['FirstName'];
      //   $customer->LastName = $request['LastName'];
      //   $customer->Address = $request['Address'];
      //   $customer->PanCard = $request['PanCard'];
      //   $customer->Phone = $request['Phone'];
      //   $customer->AdharCard = $request['AdharCard'];
      //   $customer->Referenes = $request['Referenes'];

      //   $customer->save();
    //     return redirect()->route('admin.staffcustomer.index')->with('success',' successfully added');
    // }
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = $this->customer->find($id);
        return view('admin.staffcustomer.edit', compact('customer'));   
         // $customer = Customer::find($id);
        // return response()->json($customer);
   
       // $customer = Customer::find($id);  
     // return view('staff.index', compact('staff'));  
                        // return view('staff.edit',compact('staff'));
                // return view('admin.staffcustomer.edit',compact('customer'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        $customer= $this->customer->update(request()->all(),$id);

         return redirect()->route('staffcustomer.index')
            ->with('success', 'updated successfully');
    

        //   $customer = Customer::find($id);
        // $customer->update($request->all());

        //   // dd($request->all());

        // $customer->FirstName = $request['FirstName'];
        // $customer->LastName = $request['LastName'];
        // $customer->Address = $request['Address'];
        // $customer->PanCard = $request['PanCard'];
        // $customer->Phone = $request['Phone'];
        // $customer->AdharCard = $request['AdharCard'];
        // $customer->Referenes = $request['Referenes'];


        // $customer->update();

    
        // return redirect()->route('staffcustomer.index')
        //                 ->with('success','updated successfully');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
