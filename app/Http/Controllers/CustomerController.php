<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use App\Events\SendBulk;
use Illuminate\Auth\Events\Registered;

use Illuminate\Database\Query;
use App\Http\Requests\CustomerCreateRequest;
use Maatwebsite\Excel\Facades\Excel;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Yajra\DataTables\DataTables;
// use Illuminate\Database\Eloquent\Model;
use App\Imports\CustomerImport;
use App\Exports\CustomerExport;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use Illuminate\Database\Eloquent\ModelNotFoundException;  


use Illuminate\Http\Request;
use App\Models\Customer;
use App\Repository\CustomerInterface;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response


     */
        public $customer;
    // protected $customer;

    //Here Using customer Repository
    public function __construct(CustomerInterface $customer)
    {
         $this->customer = $customer;
      
    }
//     public function __construct(Database $database)

//     {
//      $this->database = $database;
//      $this->tablename='customer1';

// }


    public function index(Request $request)
    {
 
         
        if ($request->ajax()) {
            $data = Customer::all();
           
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                       
   
                           $btn = '<a href="'.route('customer.show', ['customer'=>$row->id]).'" class="edit btn btn-info btn-sm">View</a>';
                           $btn = $btn.'&nbsp;&nbsp;<a href="'.route('customer.edit', ['customer' => $row->id]).'" class="edit btn btn-primary btn-sm">Edit</a>';

                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';

    
                           return $btn;

                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.customer.index');


        // return view('admin.customer.index', compact('customer'));
        // $customer = Customer::latest()->paginate(5);

                // $customer = User::where('role','=','admin')->get();


       

       
   //      $customer = Customer::latest()->paginate(5);
    
   //  return view('admin.customer.index',compact('customer'))
   // ->with('i', (request()->input('page', 1) - 1) * 5);
   //  }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    $customer=Customer::all();
                return view('admin.customer.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerCreateRequest $request)
    {
       
     //     $customer1=[
     //     "FirstName" => $request->FirstName,
     //     "LastName" => $request->LastName,
     //     "Address" => $request->Address,
     //     "Phone" => $request->Phone,
     //     "AdharCard" => $request->AdharCard,
     //     "PanCard" => $request->PanCard,
     //     "Referenes" => $request->Referenes,
     // ];
     //     $postRef=$this->database->getReference($this->tablename)->push($customer1);

     // //     // $postRef=$this->database->getReference($this->customer)->push($customer);
     //       $customer = new Customer;

     //    $customer->FirstName   = $request['FirstName'];
     //    $customer->LastName   = $request['LastName'];
     //    $customer->Address   = $request['Address'];
     //    $customer->Phone        = $request['Phone'];
     //    $customer->AdharCard        = $request['AdharCard'];
     //    $customer->PanCard        = $request['PanCard'];
     //    $customer->Referenes        = $request['Referenes'];

     //    $customer->save();
     //    {
        
        
        //         $customer = new Customer;

        // $customer->FirstName   = $request->FirstName;
        // $customer->LastName   = $request->LastName;
        // $customer->Address   = $request->Address;
        // $customer->Phone        = $request->Phone;
        // $customer->AdharCard        = $request->AdharCard;
        // $customer->PanCard        = $request->PanCard;
        // $customer->Referenes        = $request->Referenes;

        // $customer->save();
        
                
          

       
       

    try
    {

                $customer = new Customer;

        $customer->FirstName   = $request->FirstName;
        $customer->LastName   = $request->LastName;
        $customer->Address   = $request->Address;
        $customer->Phone        = $request->Phone;
        $customer->AdharCard        = $request->AdharCard;
        $customer->PanCard        = $request->PanCard;
        $customer->Referenes        = $request->Referenes;

        $customer->save();

            // event(new SendBulk($customer));
        event(new SendBulk($customer['FirstName'],$customer['LastName']));




            // SendBulk::dispatch($customer);






        
                

               // $this->customer->create($request->all());
           
           } catch (ModelNotFoundException $exception) {
            return $exception;
    }
        //  
        
       // $input=  $this->customer->create(request()->all());
               // $this->customer->create($request->all());

        // $this->customer->customerIncr();

       return redirect()->route('customer.index')
            ->with('success', 'saved successfully');
        
    
        // Customer::create($request->all());
     
        // return redirect()->route('customer.index')
        //                 ->with('success',' created successfully.');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $customer = $this->customer->find($id);
    // return view('admin.customer.show', compact('customer'));



    return view('admin.customer.show', compact('customer'));
}


    //     $customer = $this->customer->findOrFail($id);

    //     return view('admin.customer.show')->with(['customer' => $customer]);
    // }
         

        //         return view('admin.customer.show',compact('customer'));

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)

    {
        // $customer = Customer::find($id);
        // return response()->json($customer);

        $customer = $this->customer->find($id);
        return view('admin.customer.edit', compact('customer'));    
            // return view('admin.customer.edit',compact('customer'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerCreateRequest $request, $id)
    {
// $request->validate([
//             'FirstName' => 'required',
//             'LastName' => 'required',
//             'Address' => 'required',
//             'Phone' => 'required',
//             'AdharCard' => 'required',
//             'PanCard' => 'required',
//             'Referenes' => 'required',

//         ]);
        // $customer= $this->customer->update(request()->all(),$id);
try
{
         $customer = Customer::findOrFail($id);
        $customer->FirstName   = $request['FirstName'];
        $customer->LastName   = $request['LastName'];
        $customer->Address   = $request['Address'];
        $customer->Phone        = $request['Phone'];
        $customer->AdharCard        = $request['AdharCard'];
        $customer->PanCard        = $request['PanCard'];
        $customer->Referenes        = $request['Referenes'];

        $customer->update();
    }
         catch (ModelNotFoundException $exception) {
        return back()->withError($exception->getMessage())->withInput();

                // $this->customer->update($request->all(),$id);


}


        // $customer= $this->customer->update(request()->all());

         return redirect()->route('customer.index')
            ->with('success', 'updated successfully');
    
        
    
    //     $customer->update($request->all());
    
    //     return redirect()->route('customer.index')
    //                     ->with('success','updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Customer $customer)
    public function destroy($id)
         {

             Customer::find($id)->delete();
     
        return response()->json(['success'=>'Product deleted successfully.']);

  // $customer = Customer::findOrFail($id); 
  //       $this->customer->delete($id); 
        
        
        //  $rule=["active_status"=>0, "delete_status"=>1];
        // $customer=$this->customer->find($id);

        // print_r($customer);
        // exit();
        // if($customer->delete_status==0)
        // {

        // $customer= $this->customer->update($rule,$id);

        
        
        // }

//           $customer= Customer::find($id);
// $customer->delete();
// DB::customers('customer')->where('id', $id)->delete();

        $customer=$this->customer->find($id);
    
        return redirect()->route('customer.indexcus')
            ->with('success', 'deleted successfully');
       
        }
    



 public function export()
    {
        return Excel::download(new CustomerExport, 'customer.xlsx');


    }
    public function importform()
    {
        return view('admin.customer.import');
    }
    public function import(Request $request)
    {
        $request->validate(["file"=>"required|mimes:xlsx"]);

        $import = new CustomerImport;

        Excel::import($import, $request->file); 


        // $count =$import->getRowCount(); 

       
        
        return redirect()->route('customer.index')->with('success','Import Successfully');
    }
   

    
}





 // $delete = Customer::find($id)->delete();
        // if($delete){
        //     return redirect()->route('admin.customer.index')->with('success',' successfully deleted');
        // }


// try{
    
//         // $customer=["active_status"=>1, "delete_status"=>0];
//         $customer=$this->customer->find($id);
//          $customer = Customer::find($id);
//         // return $customer->delete_status== 0 ?? $customer->update($rule) ?? $customer->update($this->rule);
//          return redirect()->route('customer.index')->with('success',' successfully deleted');

//     }
//     catch
//         (ModelNotFoundException $exception){
//             abort(500, $exception->getMessage());

//         }
//     }
// }




        //  $customer = Customer::find($id);
        // $customer->delete();

    
        // return redirect()->route('customer.index')
        //     ->with('success', 'deleted successfully');
    //     $customer->delete();
    
    //     return redirect()->route('customer.index')
    //                     ->with('success','deleted successfully');
    // }