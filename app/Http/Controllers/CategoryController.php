<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Yajra\DataTables\DataTables;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //     public function product()
    // {
    //     return $this->hasOne(Product::class);
    //     // note: we can also inlcude Mobile model like: 'App\Mobile'
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

          if ($request->ajax()) {
            $data = Category::all();
           
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                       
   
                           $btn = '<a href="'.route('categorys.show', ['category'=>$row->id]).'" class="edit btn btn-info btn-sm">View</a>';
                           $btn = $btn.'&nbsp;&nbsp;<a href="'.route('categorys.edit', ['category' => $row->id]).'" class="edit btn btn-primary btn-sm">Edit</a>';

                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';

    
                           return $btn;

                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.categorys.indexcus');

        //  $categorys = Category::latest()->paginate(5);
    
        // return view('admin.categorys.index',compact('categorys'))
        //     ->with('i', (request()->input('page', 1) - 1) * 5);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categorys.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'name' => 'required',
            'description' => 'required',

        ]);
    
    
        Category::create($request->all());
     
        return redirect()->route('categorys.index')
                        ->with('success','created successfully.');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('admin.categorys.show',compact('category'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.categorys.edit',compact('category'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)

    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',

        ]);
    
        $category->update($request->all());
    
        return redirect()->route('categorys.index')
                        ->with('success','updated successfully');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Category $category)
    public function destroy($id)

    {
         Category::find($id)->delete();
     
        return response()->json(['success'=>' deleted successfully.']);

         // $category->delete();

    
        return redirect()->route('categorys.index')
                        ->with('success','deleted successfully');
    
    }
}
