<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class Staff1Controller extends Controller
{
    
     public function index()
    {
    	$staff=User::where('role','=','staff')->select('id','email','name','active_status')->get();
        return view('admin.staff.indexcus',compact('staff'));

    }
}
