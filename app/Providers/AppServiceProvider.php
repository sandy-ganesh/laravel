<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\CustomerRepository;
 use App\Repository\CustomerInterface;
 use App\Repository\StaffRepository;
 use App\Repository\StaffInterface;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
          $this->app->bind(CustomerInterface::class, CustomerRepository::class);
                $this->app->bind(StaffInterface::class, StaffRepository::class);
                

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
