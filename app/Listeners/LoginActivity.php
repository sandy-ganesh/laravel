<?php

namespace App\Listeners;
use Illuminate\Support\Facades\Request;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;
use App\Models\Activity;
use App\Models\User;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LoginActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
    
    $name=   $event->user->name ?  $event->user->name : " ";
        Activity::create([
            'log_name'      => 'Login/Logout',
              'description'   => $name.'  have Logged In.',
            'event'         => 'Login',
            'causer_id'     =>  $event->user->id,
            'user_agent'    =>  Request::header('User-Agent'),
            'ip_address'    =>  Request::ip()
        ]);
        
    }
    }