<?php

namespace App\Listeners;
use Illuminate\Http\Request;
use App\Events\LoginHistory;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class Login
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\LoginHistory  $event
     * @return void
     */
    public function handle(Login $event)
     {
        $name=   $event->user->name ?  $event->user->name : " ";
        Activity::create([
            'log_name'      => 'Login/Logout',
              'description'   => $name.'  have Logged In.',
            'event'         => 'LoginHistory',
            'causer_id'     =>  $event->user->id,
            'user_agent'    =>  Request::header('User-Agent'),
            'ip_address'    =>  Request::ip()
        ]);
        
    }
    }
