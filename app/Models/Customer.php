<?php

namespace App\Models;
use Illuminate\Auth\Events\SendBulk;


use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


use Haruncpi\LaravelUserActivity\Traits\Loggable;




class Customer extends Model
{
    use HasFactory,SoftDeletes;
    use Loggable;
    

    
    protected $fillable = [
        'FirstName', 'LastName','Address','Phone','AdharCard','PanCard','Referenes','active_status','delete_status'
    ];

}
