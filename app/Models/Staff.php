<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;


class Staff extends Model
{
    use HasFactory;
    use Loggable;

     protected $fillable = [
     	'FirstName', 'LastName','Address','Phone','AdharCard','PanCard','Referenes','active_status','delete_status'
        
    ];
}
