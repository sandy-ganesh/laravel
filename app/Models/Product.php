<?php

namespace App\Models;
use Haruncpi\LaravelUserActivity\Traits\Loggable;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Product extends Model
{
    use HasFactory;
    use Loggable;
    
         protected $table = 'products';

      protected $fillable = [
        'name', 'price','description',
            ];
             public function Category()
    {
         return $this->belongsToMany(Category::class,'productcategory');

    }

}
