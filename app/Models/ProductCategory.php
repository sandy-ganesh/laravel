<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;

class ProductCategory extends Model
{
    use HasFactory;
    use Loggable;
    
        protected $table = 'productcategory';


     protected $fillable = [
     	'category_id','product_id',
    ];
    
 }
