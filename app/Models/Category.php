<?php

namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;


class Category extends Model
{

    
    use HasFactory;
    use Loggable;
    
     protected $table = 'categories';

	    public $timestamps = true;

     protected $fillable = [
        'name','description',

    ];
      public function Product()
    {
        return $this->belongsToMany(Product::class,'productcategory');

    }
    
}
