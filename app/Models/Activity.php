<?php

namespace App\Models;
use App\Events\Login;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;
     protected $table = 'activity';


     protected $fillable = [ 'log_name' , 'description',  
            'event'        ,
            'causer_id'     ,
            'user_agent'    ,
            'ip_address'   , 
        ];
         protected $Login = [
        "creating" => Login::class,
        "created" => Login::class,
        
    ]; 
      
  
}
