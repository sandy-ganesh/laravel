<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Back_up extends Model
{
    use HasFactory;
         protected $table = 'back_up';


     protected $fillable = [
     	'user_id','log_date','table_name','log_type','data',
    ];
}
