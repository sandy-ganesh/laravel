<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Templeate extends Model
{
    use HasFactory;
     protected $fillable = ['templeate','message','from','to'];
}
