<?php 

namespace App\Repository;

use App\Repository\CustomerInterface;

use Kreait\Firebase\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Customer;

use PhpParser\Node\Expr\FuncCall;
use Kreait\Firebase\ServiceAccount;


class CustomerRepository implements CustomerInterface
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Customer $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all()
    {
       
        $data = $this->model->where('active_status', 1)->where('delete_status', 0)->get();

        return $data;
    }

    public function getAll()
    {
        $data = $this->model->get();

        return $data;
    }
     public function getByid($id)
    {
        return $this->model->find($id);
    }

   
    // create a new record in the database
    public function create(array $data)
    {
        return $this->model->create($data);
    }
    //find a record 
     public function find($id)
    {
        return $this->model->find($id);

    }

    // update record in the database
    public function update(array $data, $id)
    {
        // print_r($data);
        // exit;
        $record = $this->model->find($id);
        return $record->update($data);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
              return $this->model->findOrFail($id);
       
    }

   
# Connect the firebase
    public function connect()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/firebase.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://customer-c561a-default-rtdb.firebaseio.com')
            ->create();
        // Return the database

        return $database = $firebase->getDatabase();


    }

     public function customerIncr()
    {
        # increase the produuct count => +1
        $ref = $this->connect()->getReference('/customer1');
        # get old value
        $value = $ref->getvalue();
        # add the value +1
        $data = [
            'count'=>$value['count']+1,
        ];
        #update the value and store the new value ($update)
        return $update = $ref->update($data)->getValue();
    }

}
