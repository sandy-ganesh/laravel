<?php 

namespace App\Repository;
use App\Repository\StaffInterface;


use Illuminate\Support\Facades\Auth;
use DB;
use App\Models\Customer;

class StaffRepository implements StaffInterface
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Customer $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all()
    {
        $data = $this->model->where('active_status', 1)->where('delete_status', 0)->orderBy('intorder', 'ASC')->get();

        return $data;
    }

    public function getAll()
    {
        $data = $this->model->get();

        return $data;
    }

   
    // create a new record in the database
    public function create(array $data)
    {
        return $this->model->create($data);
    }
    //find a record 
     public function find($id)
    {
        return $this->model->find($id);

    }

    // update record in the database
    public function update(array $data, $id)
    {
        // print_r($data);
        // exit;
        
        $record = $this->model->find($id);
        return $record->update($data);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
              return $this->model->findOrFail($id);
       
    }

   
}