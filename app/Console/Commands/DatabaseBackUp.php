<?php

namespace App\Console\Commands;
use App\Console\Commands\Backup;
use DB;
use App\Models\Log;
use App\Models\Back_up;



use Illuminate\Console\Command;

class DatabaseBackUp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
     $count= DB::table('logs')->get();
        if(!empty($count))
        {
            foreach ($count as $value) {
                $new=new back_up ;
                $new->user_id = $value->user_id;
                $new->log_date = $value->log_date;
                $new->table_name = $value->table_name;
                $new->log_type = $value->log_type;
                $new->data = $value->data;
                $new->save();
                        $temp =Log::find($value->id)->delete();

            }
        }
        $this->info('success');
    }
}

